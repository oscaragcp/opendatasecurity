<?php

// Database Class
class Database
{

    private $db;
    private $host = "localhost";
    private $user = "homestead";
    private $pass = "secret";
    private $name = "opendata";
    static $instance;

    private function __construct()
    {
        $this->db = new PDO("mysql:host={$this->host};dbname={$this->name}", $this->user, $this->pass);
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public static function getInstance()
    {
        if (!(self::$instance instanceof self)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function query($sql)
    {
        return $this->db->query($sql);
    }

    public function prepare($statement)
    {
        return $this->db->prepare($statement);
    }
}

class Validator
{

    public function validateRequired($value)
    {
        return !empty($value);
    }

    public function validateLength($value, $min, $max)
    {
        return empty($value) || (strlen($value) >= $min && strlen($value) <= $max);
    }

    public function validateAlphabetical($value)
    {
        return preg_match("/^[a-zA-Z ]*$/", $value);
    }

    public function validateEmailFormat($value)
    {
        return filter_var($value, FILTER_VALIDATE_EMAIL);
    }

    public function validateEmailDoesNotExist($value)
    {
        $db = Database::getInstance();

        $stmt = $db->prepare("SELECT email FROM users WHERE email = ?");
        $stmt->execute([$value]);
        $email = $stmt->fetchColumn();

        return empty($email);
    }

    public function validateValuesMatch($value1, $value2)
    {
        return $value1 === $value2;
    }

    public function validateSelectNotNull($value)
    {
        return (int) $value !== 0;
    }

    public function validateChecked($value)
    {
        return $value === "on";
    }
}

// Countries Class

class Countries
{

    private $db;

    function __construct($db)
    {
        $this->db = $db;
    }

    public function getAll()
    {
        $stmt = $this->db->prepare("SELECT * FROM countries");
        $stmt->execute();

        return $stmt->fetchAll();
    }
}

// WhereHeard Class

class WhereHeard
{

    private $db;

    function __construct($db)
    {
        $this->db = $db;
    }

    public function getAll()
    {
        $stmt = $this->db->prepare("SELECT * FROM where_heard");
        $stmt->execute();

        return $stmt->fetchAll();
    }
}

// Server Side Validation

class ErrorsHandler
{

    private $validator;
    private $fields;
    private $errors;

    function __construct($fields)
    {
        $this->validator = new Validator();
        $this->fields = $fields;
        $this->errors = [];
    }

    private function requiredError($field)
    {
        return "$field is required";
    }

    private function lengthError($field, $min, $max)
    {
        return "The $field must be more than $min and less than $max characters long";
    }

    private function alphabeticalError($field)
    {
        return "The $field can only consist of alphabetical characters";
    }

    private function emailFormatError($email)
    {
        return "Email '$email' format is invalid";
    }

    private function emailExistsError($email)
    {
        return "Email '$email' already exists in the database";
    }

    private function passwordsMatchingError()
    {
        return "Password is not the same as the confirm password";
    }

    private function selectNullError($field)
    {
        return "You must select $field";
    }

    private function customErrorMsg($errorMsg)
    {
        return $errorMsg;
    }

    private function getNameErrors()
    {
        $nameErrors = [];

        if (!$this->validator->validateRequired($this->fields["name"])) {
            $nameErrors[] = $this->requiredError("Name");
        }

        if (!$this->validator->validateLength($this->fields["name"], 2, 50)) {
            $nameErrors[] = $this->lengthError("Name", 2, 50);
        }

        if (!$this->validator->validateAlphabetical($this->fields["name"])) {
            $nameErrors[] = $this->alphabeticalError("Name");
        }

        return $nameErrors;
    }

    private function getEmailErrors()
    {
        $emailErrors = [];

        if (!$this->validator->validateRequired($this->fields["email"])) {
            $emailErrors[] = $this->requiredError("email");
        }

        if (!$this->validator->validateEmailFormat($this->fields["email"])) {
            $emailErrors[] = $this->emailFormatError($this->fields["email"]);
        }

        if (!$this->validator->validateEmailDoesNotExist($this->fields["email"])) {
            $emailErrors[] = $this->emailExistsError($this->fields["email"]);
        }

        return $emailErrors;
    }

    private function getPasswordErrors()
    {
        $passwordErrors = [];

        if (!$this->validator->validateRequired($this->fields["password1"])) {
            $passwordErrors[] = $this->requiredError("Password");
        }

        if (!$this->validator->validateLength($this->fields["password1"], 6, 20)) {
            $passwordErrors[] = $this->lengthError("Password", 6, 20);
        } elseif (!$this->validator->validateValuesMatch($this->fields["password1"], $this->fields["password2"])) {
            $passwordErrors[] = $this->passwordsMatchingError();
        }

        return $passwordErrors;
    }

    private function getAddressErrors()
    {
        $addressErrors = [];

        if (!$this->validator->validateRequired($this->fields["address1"])) {
            $addressErrors[] = $this->requiredError("Address 1");
        }

        if (!$this->validator->validateLength($this->fields["address1"], 2, 80)) {
            $addressErrors[] = $this->lengthError("Address 1", 2, 80);
        }

        if (!$this->validator->validateLength($this->fields["address2"], 2, 80)) {
            $addressErrors[] = $this->lengthError("Address 2", 2, 80);
        }

        if (!$this->validator->validateLength($this->fields["address3"], 2, 80)) {
            $addressErrors[] = $this->lengthError("Address 3", 2, 80);
        }

        return $addressErrors;
    }

    private function getTownCityErrors()
    {
        $townCityErrors = [];

        if (!$this->validator->validateRequired($this->fields["towncity"])) {
            $townCityErrors[] = $this->requiredError("Town/City");
        }

        if (!$this->validator->validateLength($this->fields["towncity"], 2, 30)) {
            $townCityErrors[] = $this->lengthError("Town/City", 2, 30);
        }

        return $townCityErrors;
    }

    private function getCountyRegionErrors()
    {
        $countyRegionErrors = [];

        if (!$this->validator->validateRequired($this->fields["countyregion"])) {
            $countyRegionErrors[] = $this->requiredError("County/Region");
        }

        if (!$this->validator->validateLength($this->fields["countyregion"], 2, 30)) {
            $countyRegionErrors[] = $this->lengthError("County/Region", 2, 30);
        }

        return $countyRegionErrors;
    }

    private function getCountryErrors()
    {
        $countryErrors = [];

        if (!$this->validator->validateSelectNotNull($this->fields["country"])) {
            $countryErrors[] = $this->selectNullError("a Country");
        }

        return $countryErrors;
    }

    private function getWhereHeardErrors()
    {
        $whereHeardErrors = [];

        if (!$this->validator->validateSelectNotNull($this->fields["whereheard"])) {
            $whereHeardErrors[] = $this->selectNullError("where did you hear about us");
        }

        return $whereHeardErrors;
    }

    private function getWhereHeardOtherErrors()
    {
        $whereHeardOther = [];

        if ((int) $this->fields["whereheard"] === 7) {
            if (!$this->validator->validateRequired($this->fields["whereheardother"])) {
                $whereHeardOther[] = $this->requiredError("Where did you hear about us (Other)");
            }
        }

        return $whereHeardOther;
    }

    private function getTermsErrors()
    {
        $termsErrors = [];

        if (!$this->validator->validateChecked($this->fields["terms"])) {
            $termsErrors[] = $this->customErrorMsg("Terms and conditions must be accepted");
        }

        return $termsErrors;
    }

    public function getErrors()
    {
        $this->errors = array_merge(
            $this->getNameErrors()
            , $this->getEmailErrors()
            , $this->getPasswordErrors()
            , $this->getAddressErrors()
            , $this->getTownCityErrors()
            , $this->getCountyRegionErrors()
            , $this->getCountryErrors()
            , $this->getWhereHeardErrors()
            , $this->getWhereHeardOtherErrors()
            , $this->getTermsErrors());

        return array_filter($this->errors);
    }
}

class User
{

    private $db;
    private $name;
    private $email;
    private $password;
    private $address1;
    private $address2;
    private $address3;
    private $townCity;
    private $countyRegion;
    private $countryNumericCode;
    private $whereHeard;
    private $whereHeardOther;

    function __construct($db)
    {
        $this->db = $db;
    }

    public function register($params)
    {
        $this->name = $params['name'];
        $this->email = $params['email'];
        $this->password = $params['password1'];
        $this->address1 = $params['address1'];
        $this->address2 = $params['address2'];
        $this->address3 = $params['address3'];
        $this->townCity = $params['towncity'];
        $this->countyRegion = $params['countyregion'];
        $this->countryNumericCode = $params['country'];
        $this->whereHeard = $params['whereheard'];
        $this->whereHeardOther = $params['whereheardother'];

        try {
            $stmt = $this->db->prepare("INSERT INTO users(name, email, password, address1, address2, address3, town_city, county_region, where_heard_id, where_heard_other, country_numeric_code)
                                                       VALUES(:name, :email, :password, :address1, :address2, :address3, :town_city, :county_region, :where_heard_id, :where_heard_other, :country_numeric_code)");

            $stmt->bindparam(":name", $this->name);
            $stmt->bindparam(":email", $this->email);
            $passwordHashed = password_hash($this->password, PASSWORD_BCRYPT);
            $stmt->bindparam(":password", $passwordHashed);
            $stmt->bindparam(":address1", $this->address1);
            $stmt->bindparam(":address2", $this->address2);
            $stmt->bindparam(":address3", $this->address3);
            $stmt->bindparam(":town_city", $this->townCity);
            $stmt->bindparam(":county_region", $this->countyRegion);
            $stmt->bindparam(":where_heard_id", $this->whereHeard);
            $stmt->bindparam(":where_heard_other", $this->whereHeardOther);
            $stmt->bindparam(":country_numeric_code", $this->countryNumericCode);

            $stmt->execute();

            return $stmt;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function login($email, $password)
    {
        try {
            $stmt = $this->db->prepare("SELECT * FROM users WHERE email=:email LIMIT 1");
            $stmt->execute(array(':email' => $email));
            $userRow = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($stmt->rowCount() > 0) {
                if (password_verify($password, $userRow['password'])) {
                    $_SESSION['user_session'] = $userRow['id'];
                    return true;
                } else {
                    return false;
                }
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}

$errors = [];

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $fields["name"] = filter_input(INPUT_POST, "name");
    $fields["email"] = filter_input(INPUT_POST, "email");
    $fields["password1"] = filter_input(INPUT_POST, "password1");
    $fields["password2"] = filter_input(INPUT_POST, "password2");
    $fields["address1"] = filter_input(INPUT_POST, "address1");
    $fields["address2"] = filter_input(INPUT_POST, "address2");
    $fields["address3"] = filter_input(INPUT_POST, "address3");
    $fields["towncity"] = filter_input(INPUT_POST, "towncity");
    $fields["countyregion"] = filter_input(INPUT_POST, "countyregion");
    $fields["country"] = filter_input(INPUT_POST, "country");
    $fields["whereheard"] = filter_input(INPUT_POST, "whereheard");
    $fields["whereheardother"] = filter_input(INPUT_POST, "whereheardother");
    $fields["terms"] = filter_input(INPUT_POST, "terms");

    $errorHandler = new ErrorsHandler($fields);

    $errors = $errorHandler->getErrors();

    if (count($errors) === 0) {
        // Create register in database
        $user = new User(Database::getInstance());
        $user->register($fields);
        // Log in
        $loginResult = $user->login($fields['email'], $fields['password1']);
        // Redirect to success page
        if ($loginResult) {
            header('Location: success.php', true);
        exit();
        } else {
            $errors[] = 'There was a problem. It was not possible to log you in.';
        }
        
    }
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>eCommerce Sign Up</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- FormValidation CSS file -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.formvalidation/0.6.1/css/formValidation.min.css">

        <!-- jQuery v1.9.1 -->
        <script src="https://code.jquery.com/jquery-1.9.1.min.js" integrity="sha256-wS9gmOZBqsqWxgIVgA8Y9WcQOa7PgSIX+rPA0VL2rbQ=" crossorigin="anonymous"></script>

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

        <!-- FormValidation plugin and the class supports validating Bootstrap form -->
        <script src="https://cdn.jsdelivr.net/jquery.formvalidation/0.6.1/js/formValidation.min.js"></script>
        <script src="https://cdn.jsdelivr.net/jquery.formvalidation/0.6.1/js/framework/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container">
            <div class="page-header">
                <h1>eCommerce</h1>
                <p class="lead">Sign Up</p>
            </div>

            <div class="row">
                <div class="col-md-9">
                    <?php if (count($errors) > 0): ?>
                        <div>
                            <div class="alert alert-danger" role="alert"><ul><?php
                                    foreach ($errors as $error) {
                                        echo "<li>$error</li>";
                                    }

                                    ?></ul></div>
                        </div>
                    <?php endif; ?>

                    <form id="signUpForm" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                        <div class="form-group">
                            <label for="inputName">Name</label>
                            <input type="text" name="name" value="<?php echo filter_input(INPUT_POST, "name"); ?>" class="form-control" id="inputName" placeholder="Name">
                        </div>
                        <div class="form-group">
                            <label for="inputEmail1">Email address</label>
                            <input type="email" name="email" value="<?php echo filter_input(INPUT_POST, "email"); ?>" class="form-control" id="inputEmail1" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label for="inputPassword1">Password</label>
                            <input type="password" name="password1" class="form-control" id="inputPassword1" placeholder="Password">
                        </div>
                        <div class="form-group">
                            <label for="inputPassword2">Confirm Password</label>
                            <input type="password" name="password2" class="form-control" id="inputPassword2" placeholder="Confirm Password">
                        </div>
                        <div class="form-group">
                            <label for="inputAddress1">Address 1</label>
                            <input type="text" name="address1" value="<?php echo filter_input(INPUT_POST, "address1"); ?>" class="form-control" id="inputAddress1" placeholder="Address 1">
                        </div>
                        <div class="form-group">
                            <label for="inputAddress2">Address 2</label>
                            <input type="text" name="address2" value="<?php echo filter_input(INPUT_POST, "address2"); ?>" class="form-control" id="inputAddress2" placeholder="Address 2">
                        </div>
                        <div class="form-group">
                            <label for="inputAddress3">Address 3</label>
                            <input type="text" name="address3" value="<?php echo filter_input(INPUT_POST, "address3"); ?>" class="form-control" id="inputAddress3" placeholder="Address 3">
                        </div>
                        <div class="form-group">
                            <label for="inputTownCity">Town/City</label>
                            <input type="text" name="towncity" value="<?php echo filter_input(INPUT_POST, "towncity"); ?>" class="form-control" id="inputTownCity" placeholder="Town/City">
                        </div>
                        <div class="form-group">
                            <label for="inputCountyRegion">County/Region</label>
                            <input type="text" name="countyregion" value="<?php echo filter_input(INPUT_POST, "countyregion"); ?>" class="form-control" id="inputCountyRegion" placeholder="County/Region">
                        </div>
                        <div class="form-group">
                            <label for="selectCountry">Country</label>
                            <select class="form-control" name="country" id="selectCountry">
                                <option value="0">-</option>
                                <?php
                                $countries = new Countries(Database::getInstance());
                                foreach ($countries->getAll() as $country):

                                    ?>
                                    <option value="<?php echo $country['numeric_code'] ?>" <?php echo filter_input(INPUT_POST, "country") === $country['numeric_code'] ? "selected" : ""; ?>><?php echo utf8_encode($country['name']) ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="selectWhereHeard">Where did you hear abour us</label>
                            <select class="form-control" name="whereheard" id="selectWhereHeard">
                                <option value="0">-</option>
                                <?php
                                $whereHeard = new WhereHeard(Database::getInstance());
                                foreach ($whereHeard->getAll() as $whereHeardOption):

                                    ?>
                                    <option value="<?php echo $whereHeardOption['id'] ?>" <?php echo filter_input(INPUT_POST, "whereheard") === $whereHeardOption['id'] ? "selected" : ""; ?>><?php echo utf8_encode($whereHeardOption['name']) ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group" id="formGroupWhereHeardOther" <?php echo filter_input(INPUT_POST, "whereheard") === 0 ? "" : "hidden" ?>>
                            <label for="textareaWhereHeardOther">Where did you hear about us (Other)</label>
                            <textarea class="form-control" name="whereheardother" id="textareaWhereHeardOther" rows="3" readonly><?php echo filter_input(INPUT_POST, "whereheardother"); ?></textarea>
                        </div>
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="terms" <?php echo filter_input(INPUT_POST, "terms") === "on" ? "checked" : ""; ?>> Agree with the terms and conditions
                                </label>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>

                    <script>
                        $(document).ready(function () {
                            $('#signUpForm').formValidation({
                                framework: 'bootstrap',
                                icon: {
                                    valid: 'glyphicon glyphicon-ok',
                                    invalid: 'glyphicon glyphicon-remove',
                                    validating: 'glyphicon glyphicon-refresh'
                                },
                                fields: {
                                    name: {
                                        validators: {
                                            notEmpty: {
                                                message: 'Name is required'
                                            },
                                            stringLength: {
                                                min: 2,
                                                max: 50,
                                                message: 'Name must be more than 2 and less than 50 characters long'
                                            },
                                            regexp: {
                                                regexp: /^[a-zA-Z ]+$/,
                                                message: 'Name can only consist of alphabetical characters'
                                            }
                                        }
                                    },
                                    email: {
                                        validators: {
                                            notEmpty: {
                                                message: 'Email address is required'
                                            },
                                            emailAddress: {
                                                message: 'The input is not a valid email address'
                                            }
                                        }
                                    },
                                    password1: {
                                        validators: {
                                            notEmpty: {
                                                message: 'Password is required'
                                            },
                                            stringLength: {
                                                min: 6,
                                                max: 20,
                                                message: 'Password must be more than 6 and less than 20 characters long'
                                            }
                                        }
                                    },
                                    password2: {
                                        validators: {
                                            notEmpty: {
                                                message: 'Confirm Password is required'
                                            },
                                            stringLength: {
                                                min: 6,
                                                max: 20,
                                                message: 'Cofirm Password must be more than 6 and less than 20 characters long'
                                            },
                                            identical: {
                                                field: 'password1',
                                                message: 'Password and its confirm are not the same'
                                            }
                                        }
                                    },
                                    address1: {
                                        validators: {
                                            notEmpty: {
                                                message: 'Address 1 is required'
                                            },
                                            stringLength: {
                                                min: 2,
                                                max: 80,
                                                message: 'Address 1 must be more than 2 and less than 80 characters long'
                                            }
                                        }
                                    },
                                    address2: {
                                        validators: {
                                            stringLength: {
                                                min: 2,
                                                max: 80,
                                                message: 'Address 2 must be more than 2 and less than 80 characters long'
                                            }
                                        }
                                    },
                                    address3: {
                                        validators: {
                                            stringLength: {
                                                min: 2,
                                                max: 80,
                                                message: 'Address 3 must be more than 2 and less than 80 characters long'
                                            }
                                        }
                                    },
                                    towncity: {
                                        validators: {
                                            notEmpty: {
                                                message: 'Town/City is required'
                                            },
                                            stringLength: {
                                                min: 2,
                                                max: 30,
                                                message: 'Town/City must be more than 2 and less than 30 characters long'
                                            }
                                        }
                                    },
                                    countyregion: {
                                        validators: {
                                            notEmpty: {
                                                message: 'The County/Region is required'
                                            },
                                            stringLength: {
                                                min: 2,
                                                max: 30,
                                                message: 'County/Region must be more than 2 and less than 30 characters long'
                                            }
                                        }
                                    },
                                    country: {
                                        validators: {
                                            between: {
                                                min: 1,
                                                max: 894,
                                                message: 'You must select a Contry'
                                            }
                                        }
                                    },
                                    whereheard: {
                                        validators: {
                                            between: {
                                                min: 1,
                                                max: 7,
                                                message: 'You must select where did you hear about us'
                                            }
                                        }
                                    },
                                    whereheardother: {
                                        validators: {
                                            callback: {
                                                message: 'Please specific where did you hear about us',
                                                callback: function (value, validator, $field) {
                                                    var channel = $('#signUpForm').find('select#selectWhereHeard option:selected').val();
                                                    return (parseInt(channel) !== 7) ? true : (value !== '');
                                                }
                                            }
                                        }
                                    },
                                    terms: {
                                        validators: {
                                            notEmpty: {
                                                message: 'You must agree with the terms and conditions'
                                            }
                                        }
                                    }
                                }
                            }).on('change', '[name="whereheard"]', function (e) {
                                $('#signUpForm').formValidation('revalidateField', 'whereheardother');
                            }).on('success.field.fv', function (e, data) {
                                if (data.field === 'whereheardother') {
                                    var channel = $('#signUpForm').find('select#selectWhereHeard option:selected').val();
                                    // User choose given channel
                                    if (parseInt(channel) !== 7) {
                                        // Remove the success class from the container
                                        data.element.closest('.form-group').removeClass('has-success');
                                        // Hide the tick icon
                                        data.element.data('fv.icon').hide();
                                    }
                                }
                            });

                            // Select Where Heard Other TextArea activation
                            $("select#selectWhereHeard")
                                .change(function () {
                                    if ($("select#selectWhereHeard option:selected").val() == 7) {
                                        $("textarea#textareaWhereHeardOther").removeAttr('readonly');
                                        $("div#formGroupWhereHeardOther").show();
                                    } else {
                                        $("textarea#textareaWhereHeardOther").attr('readonly', 'readonly');
                                        $("div#formGroupWhereHeardOther").hide();
                                    }
                                });
                        });
                    </script>

                </div>
            </div>
        </div>
    </body>
</html>