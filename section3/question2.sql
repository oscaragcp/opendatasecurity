-- Question 2:
-- Write an MySQL command to perform a search on the database
-- that you have designed to find all films that include the keyword
-- “America” within the genre “Action” (genre ID = 126).

SELECT	f.*
FROM	films AS f
		INNER JOIN films_genres AS fg
			ON f.id = fg.film_id
		INNER JOIN genres AS g
			ON g.id = fg.genre_id
WHERE	f.title LIKE '%America%' AND g.id = 126;
